import graphene

from .queries import ArtistType
from ..models import Artist


class ArtistInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()


class CreateArtist(graphene.Mutation):
    class Arguments:
        input = ArtistInput(required=True)

    artist = graphene.Field(ArtistType)

    @staticmethod
    def mutate(root, info, input=None):
        artist_instance = Artist(name=input.name)
        artist_instance.save()
        return CreateArtist(artist=artist_instance)


class Mutation(graphene.ObjectType):
    create_artist = CreateArtist.Field()
