import graphene
from graphene_django.types import DjangoObjectType, ObjectType

from ..models import *


class SongType(DjangoObjectType):
    class Meta:
        model = Song
        fields = "__all__"


class AlbumType(DjangoObjectType):
    class Meta:
        model = Album
        fields = "__all__"


class InstrumentType(DjangoObjectType):
    class Meta:
        model = Instrument
        fields = "__all__"


class TagType(DjangoObjectType):
    class Meta:
        model = Tag
        fields = "__all__"


class SubgenereType(DjangoObjectType):
    class Meta:
        model = Subgenere
        fields = "__all__"


class GenereType(DjangoObjectType):
    class Meta:
        model = Genere
        fields = "__all__"


class BandType(DjangoObjectType):
    class Meta:
        model = Band
        fields = "__all__"


class SongFilterType(DjangoObjectType):
    class Meta:
        model = Song


class ArtistType(DjangoObjectType):
    class Meta:
        model = Artist


class Query(ObjectType):
    allInstruments = graphene.List(InstrumentType)
    allTags = graphene.List(TagType)
    allSubgeneres = graphene.List(SubgenereType)
    allGeneres = graphene.List(GenereType)
    allBands = graphene.List(BandType)

    allSongs = graphene.List(SongType)
    allArtists = graphene.List(ArtistType)
    songsSearch = graphene.List(SongFilterType, string=graphene.String())

    def resolve_allInstruments(self, info, **kwargs):
        return Instrument.objects.all()

    def resolve_allTags(self, info, **kwargs):
        return Tag.objects.all()

    def resolve_allSubgeneres(self, info, **kwargs):
        return Subgenere.objects.all()

    def resolve_allGeneres(self, info, **kwargs):
        return Genere.objects.all()

    def resolve_allBands(self, info, **kwargs):
        return Band.objects.all()

    def resolve_allSongs(self, info, **kwargs):
        return Song.objects.all()

    def resolve_allArtists(self, info, **kwargs):
        return Artist.objects.all()

    def resolve_allAlbums(self, info, **kwargs):
        return Album.objects.all()

    def resolve_songSsearch(self, info, **kwargs):
        string = kwargs.get("string", "")
        return Song.objects.filter(genere_name__icontains=string)
