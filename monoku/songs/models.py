from django.db import models


class Instrument(models.Model):
    name = models.CharField(max_length=100,  unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Tag(models.Model):
    name = models.CharField(max_length=100,  unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Subgenere(models.Model):
    name = models.CharField(max_length=100,  unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Genere(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Artist(models.Model):
    name = models.CharField(max_length=100,  unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Band(models.Model):
    name = models.CharField(max_length=100,  unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Album(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    band = models.ForeignKey(Band, on_delete=models.CASCADE, related_name="band")
    similar_bands = models.ManyToManyField(Band, related_name="similar_bands")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )


class Song(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    genere = models.ForeignKey(Genere, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    instruments = models.ManyToManyField(Instrument)
    duration = models.DurationField()
    external_id = models.CharField(max_length=250, blank=True, null=True)
    subgeneres = models.ManyToManyField(Subgenere)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )

class Import(models.Model):
    # Choice definitions
    SUCCESS = 'ok'
    FAIL = 'fail'

    STATE_CHOICE = [
        (SUCCESS, 'Success'),
        (FAIL, 'Failed'),
    ]
    create = models.DateField(auto_now=True)
    state = models.CharField(choices=STATE_CHOICE,max_length=20)

    def __str__(self):
        return self.state

    class Meta:
        ordering = ('create', )
