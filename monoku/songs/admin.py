from django.contrib import admin
from import_export.admin import ImportMixin
from .models import Instrument, Tag, Subgenere, Genere, Artist, Band, Album, Song, Import


# Register your models here.
@admin.register(Instrument)
class InstrumentAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Subgenere)
class SubgenereAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Genere)
class GenereAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Band)
class BandAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(Import)
class ImportResources(ImportMixin, admin.ModelAdmin):
    presource_class = Import

    
