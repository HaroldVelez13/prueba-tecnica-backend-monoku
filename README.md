# [**Prueba técnica backend - Monoku 2021 🎶**](https://www.notion.so/Prueba-t-cnica-backend-Monoku-2021-c11279a253714ba5be261538e2ea99fe)

## Tecnologies

* ###  Django 3.1
* ### postgres 10
* ### API GraphQL with **graphene_django**
* ### Docker container
* ### Docker compose

## Init
* Run docker-compose up --build
* docker exec  -it --user=root backend_mn python manage.py makemigrations 
* docker exec  -it --user=root backend_mn python manage.py migrate 
* docker exec  -it --user=root backend_mn python manage.py createsuperuser 


# Query examples

* query{
    allSongs{
        name,
        tags{name}
    }

* query{
    allSongs{
        name,
        genere{name}
    }

# Mutation Example 

* mutation{
    createArtist(input:{name:"other Artis"}){
        artist{
            id,
            name
        }
    }
}